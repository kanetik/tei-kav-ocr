package gr.edu.telkav.ocr.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.util.concurrent.Callable;

import gr.edu.telkav.ocr.Configuration;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Uses tesseract OCR engine to process images and extract text
 */
public class ImageProcessor {

    private final TessBaseAPI mTessBaseAPI;
    private final BitmapTransformer mBitmapTransformer;
    private boolean isInit;

    public ImageProcessor(TessBaseAPI tessBaseAPI, BitmapTransformer bitmapTransformer) {
        mTessBaseAPI = tessBaseAPI;
        mBitmapTransformer = bitmapTransformer;
    }

    /**
     * Converts image into greyscale for easier OCR engine decoding
     * @param imgUri URI of image to be postProcessed
     * @return postprocessed image for easier OCR engine decoding
     */
    public Observable<Bitmap> postProcessImage(final Uri imgUri) {
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4; // 1 - means max size. 4 - means maxsize/4 size. Don't use value <4, because you need more memory in the heap to store your data.
                Bitmap bitmap = BitmapFactory.decodeFile(imgUri.getPath(), options);
                return mBitmapTransformer.postProcessBitmap(bitmap);
            }
        }).subscribeOn(Schedulers.io());
    }

    /**
     * Extracts text from paragraph, after text is extracted engine frees up resources
     *
     * @param bitmap image to extract text from
     * @return extracted text from image containing paragraph of words
     */
    public String extractText(Bitmap bitmap) {
        mTessBaseAPI.init(Configuration.DATA_PATH, Configuration.LANGUAGE);
        mTessBaseAPI.setImage(bitmap);
        String extractedText = "empty result";
        try {
            extractedText = mTessBaseAPI.getUTF8Text();
        } catch (Exception e) {
            Timber.e("Error in recognizing text.");
        }
        mTessBaseAPI.end();
        return extractedText;
    }

    /**
     * Configured to extract only single line of text
     */
    public String extractTextContinuous(Bitmap data) {
        if (!isInit) {
            mTessBaseAPI.init(Configuration.DATA_PATH, Configuration.LANGUAGE);
            mTessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
            mTessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "οΟ'");
            isInit = true;
        }

        Bitmap bitmap = mBitmapTransformer.postProcessBitmap(data);

        String extractedText = "empty result";
        try {
            mTessBaseAPI.setImage(bitmap);
            try {
                extractedText = mTessBaseAPI.getUTF8Text();
            } catch (Exception e) {
                Timber.e("Error in recognizing text.");
            }
            Timber.w(extractedText);
        } catch (IllegalStateException ex) {
            Timber.e(ex);
        }

        return extractedText;
    }

    /**
     * To be called after {@link #extractTextContinuous(Bitmap)} is no longer being called
     * Frees up resources and prevents memory leaks
     */
    public void closeApi() {
        mTessBaseAPI.end();
        isInit = false;
    }
}
