package gr.edu.telkav.ocr.view.screens.continuous;

import android.support.annotation.NonNull;

import com.otaliastudios.cameraview.Frame;
import com.otaliastudios.cameraview.FrameProcessor;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gr.edu.telkav.ocr.presenter.CameraPresenter;
import timber.log.Timber;

/**
 * Processor capturing preview frames in separate thread.
 * Uses Reentrant lock to prevent multiple threads processing at once, causing memory
 * problems. Uses 500ms delay between frames to improve performance.
 */
public class ContinuousFrameProcessor implements FrameProcessor {
    private static final int FRAME_PROCESSING_DELAY_MILLIS = 500;

    private final Lock mFrameLock = new ReentrantLock();
    private boolean mProcessing = false;
    private long mLastPreviewProcessed = 0;
    private CameraPresenter mCameraPresenter;

    ContinuousFrameProcessor(CameraPresenter cameraPresenter) {
        mCameraPresenter = cameraPresenter;
    }

    @Override
    public void process(@NonNull Frame frame) {
        if (mProcessing) {
            return;
        }
        long startMillis = System.currentTimeMillis();
        if (startMillis - mLastPreviewProcessed < FRAME_PROCESSING_DELAY_MILLIS) {
            return;
        }

        if (mFrameLock.tryLock()) {
            mProcessing = true;
            Frame lockFrame = null;
            try {
                lockFrame = frame.freeze();
                Timber.d("process started %d", startMillis);
                mCameraPresenter.decodePreviewFrame(lockFrame);
                mLastPreviewProcessed = System.currentTimeMillis();
                Timber.d("process ended %d", mLastPreviewProcessed - startMillis);

            } catch (Exception e) {
                Timber.e(e);
            } finally {
                if (lockFrame != null) lockFrame.release();
                frame.release();
                mProcessing = false;
                mFrameLock.unlock();
            }
        }
    }
}
