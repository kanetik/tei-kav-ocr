package gr.edu.telkav.ocr.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.util.Pair;

import com.cv4j.core.binary.Threshold;
import com.cv4j.core.datamodel.ByteProcessor;
import com.cv4j.core.datamodel.CV4JImage;

/**
 * Helper class containing methods for image manipulation and conversion
 */
public class BitmapTransformer {

    private Rect mCropFrame;
    private Point mImageSize;

    public BitmapTransformer() {
    }

    public Pair<Rect, Point> getCropFrame() {
        return new Pair<>(mCropFrame, mImageSize);
    }

    public Bitmap rotateAndCropBitmap(final Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        final int imgWidth = 100;
        final int startx = source.getWidth() / 2 - imgWidth / 2;
        final int starty = imgWidth / 2;
        final int offsetx = imgWidth;
        final int offsety = source.getHeight() - imgWidth;

        //image crop size
        mCropFrame = new Rect(
                starty,
                startx+imgWidth,
                starty+offsety,
                startx+offsetx+imgWidth
        );

        Bitmap bitmap = Bitmap.createBitmap(source, startx, starty
                , offsetx, offsety, matrix, true);

        mImageSize = new Point(Math.min(source.getWidth(), source.getHeight()), Math.max(source.getWidth(), source.getHeight()));

        return bitmap;
    }

    public Allocation renderScriptNV21ToRGBA888(Context context, int width, int height, byte[] nv21) {
        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicYuvToRGB yuvToRgbIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));

        Type.Builder yuvType = new Type.Builder(rs, Element.U8(rs)).setX(nv21.length);
        Allocation in = Allocation.createTyped(rs, yuvType.create(), Allocation.USAGE_SCRIPT);

        Type.Builder rgbaType = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(width).setY(height);
        Allocation out = Allocation.createTyped(rs, rgbaType.create(), Allocation.USAGE_SCRIPT);

        in.copyFrom(nv21);

        yuvToRgbIntrinsic.setInput(in);
        yuvToRgbIntrinsic.forEach(out);
        return out;
    }

    public Bitmap postProcessBitmap(Bitmap bitmap) {
        CV4JImage cv4JImage = new CV4JImage(bitmap);
        Threshold threshold = new Threshold();
        threshold.adaptiveThresh((ByteProcessor) (cv4JImage.convert2Gray().getProcessor()), Threshold.ADAPTIVE_C_MEANS_THRESH, 12, 30, Threshold.METHOD_THRESH_BINARY);

        return cv4JImage.getProcessor().getImage().toBitmap(Bitmap.Config.ARGB_8888);
    }
}
