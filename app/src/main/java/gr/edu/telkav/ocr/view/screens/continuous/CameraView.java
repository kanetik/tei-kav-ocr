package gr.edu.telkav.ocr.view.screens.continuous;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Pair;

import gr.edu.telkav.ocr.view.BaseViewContract;

public interface CameraView extends BaseViewContract {

    void setExtractedText(String extractedText);

    void setPreviewBitmap(Bitmap bitmap);

    void setPreviewFrame(Pair<Rect, Point> frame);
}
