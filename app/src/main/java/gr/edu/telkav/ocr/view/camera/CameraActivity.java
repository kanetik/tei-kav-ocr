package gr.edu.telkav.ocr.view.camera;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.cameraview.AspectRatio;
import com.google.android.cameraview.CameraView;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.Callable;

import gr.edu.telkav.ocr.R;
import gr.edu.telkav.ocr.view.camera.util.DisplayUtils;
import gr.edu.telkav.ocr.view.camera.util.FileUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static gr.edu.telkav.ocr.R.id.*;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    private CameraView mCameraView;

    private float mRatio;
    private float mCameraRatio;

    private ImageButton mButtonCapture;
    private ProgressBar mProgressBar;

    private Point mRectPictureSize;
    private Uri mImageUri;
    private String mImagePath;
    private final CameraView.Callback mCallback = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            super.onCameraOpened(cameraView);
            Timber.d( "onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            super.onCameraClosed(cameraView);
            Timber.d( "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {
            Timber.d( "onPictureTaken %d", data.length);
            Observable.fromCallable(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return saveBitmap(data);
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mButtonCapture.setVisibility(View.GONE);
                            mProgressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onNext(Boolean aBoolean) {

                        }

                        @Override
                        public void onError(Throwable e) {
                            finish();
                        }

                        @Override
                        public void onComplete() {
                            finish();
                        }
                    });


        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        setupViews(getIntent());
        initView(camera_view);
        mCameraView.addCallback(mCallback);

    }

    private void setupViews(@NonNull Intent mIntent) {
        mRatio = mIntent.getFloatExtra(EasyCamera.EXTRA_VIEW_RATIO, 1f);
        mImageUri = mIntent.getParcelableExtra(EasyCamera.EXTRA_OUTPUT_URI);
        mImagePath = FileUtils.getRealFilePath(this, mImageUri);
    }

    private void initView(int camera_view) {
        mCameraView = findViewById(camera_view);
        mButtonCapture = findViewById(ibt_capture);
        ImageView ivReturn = findViewById(iv_return);
        mProgressBar = findViewById(progress);

        mButtonCapture.setOnClickListener(this);
        ivReturn.setOnClickListener(this);

        AspectRatio currentRatio = mCameraView.getAspectRatio();
        mCameraRatio = currentRatio.toFloat();
        int cameraWidth = (int) DisplayUtils.getScreenWidth(this);
        int cameraHeight = (int) (cameraWidth * mCameraRatio);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.width = cameraWidth;
        layoutParams.height = cameraHeight;
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
    }

    @Override
    protected void onPause() {
        mCameraView.stop();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            mCameraView.start();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    private boolean saveBitmap(final byte[] data) {
        Bitmap bitmap;
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(new ByteArrayInputStream(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

        bitmap = rotate(BitmapFactory.decodeByteArray(data, 0, data.length), orientation);
        if (bitmap != null) {
            if (mRectPictureSize == null) {
                mRectPictureSize = DisplayUtils.createCenterPictureRect(mRatio, mCameraRatio, bitmap.getWidth(), bitmap.getHeight());
            }
            int x = bitmap.getWidth() / 2 - mRectPictureSize.x / 2;
            int y = bitmap.getHeight() / 2 - mRectPictureSize.y / 2;
            Bitmap rectBitmap = Bitmap.createBitmap(bitmap, x, y, mRectPictureSize.x, mRectPictureSize.y);
            int imageWidth = rectBitmap.getWidth();
            int imageHeight = rectBitmap.getHeight();
            FileUtils.saveBitmap(rectBitmap, mImagePath);
            setResultUri(mImageUri, imageWidth, imageHeight);

            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (!rectBitmap.isRecycled()) {
                rectBitmap.recycle();
            }
            return true;
        } else {
            return false;
        }
    }

    public Bitmap rotate(Bitmap in, int orientation) {
        int rotationDegrees = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotationDegrees = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotationDegrees = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotationDegrees = 270;
                break;
        }

        Matrix mat = new Matrix();
        mat.postRotate(rotationDegrees);
        return Bitmap.createBitmap(in, 0, 0, in.getWidth(), in.getHeight(), mat, true);
    }

    protected void setResultUri(Uri uri, int imageWidth, int imageHeight) {
        setResult(RESULT_OK, new Intent()
                .putExtra(EasyCamera.EXTRA_OUTPUT_URI, uri)
                .putExtra(EasyCamera.EXTRA_OUTPUT_IMAGE_WIDTH, imageWidth)
                .putExtra(EasyCamera.EXTRA_OUTPUT_IMAGE_HEIGHT, imageHeight)
        );
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == ibt_capture) {
            mCameraView.takePicture();
        } else if (i == iv_return) {
            CameraActivity.this.finish();
        }
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                               @NonNull int[] grantResults) {
            switch (requestCode) {
                case REQUEST_CAMERA_PERMISSION:
                    if (permissions.length != 1 || grantResults.length != 1) {
                        throw new RuntimeException(getString(R.string.error_camera_permission));
                    }
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), R.string.camera_permission_not_granted, Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

    }

}
