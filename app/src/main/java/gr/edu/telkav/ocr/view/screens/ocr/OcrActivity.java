package gr.edu.telkav.ocr.view.screens.ocr;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import gr.edu.telkav.ocr.ObjectFactory;
import gr.edu.telkav.ocr.R;
import gr.edu.telkav.ocr.presenter.OcrPresenter;
import gr.edu.telkav.ocr.view.BaseActivity;
import gr.edu.telkav.ocr.view.camera.EasyCamera;
import timber.log.Timber;

public class OcrActivity extends BaseActivity<OcrPresenter> implements OcrView {

    TextView mExtractedText;
    ImageView mImageView;
    FloatingActionButton mFloatingActionButton;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);
        initViews();
    }

    private void initViews() {
        mExtractedText = findViewById(R.id.text);
        mFloatingActionButton = findViewById(R.id.fab);
        mImageView = findViewById(R.id.imageView);
        mProgressBar = findViewById(R.id.progress);

        mExtractedText.setClickable(true);
        mExtractedText.setMovementMethod(LinkMovementMethod.getInstance());

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String SAMPLE_CROPPED_IMAGE_NAME = "cropImage_" + System.currentTimeMillis() + ".png";
                Uri destination = Uri.fromFile(new File(getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME));
                EasyCamera.create(destination)
                        .withViewRatio(0.5f)
                        .withMarginCameraEdge(50, 50)
                        .start(OcrActivity.this);
            }
        });
    }

    @Override
    protected OcrPresenter createPresenter() {
        return ObjectFactory.getInstance().getOcrPresenter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == EasyCamera.REQUEST_CAPTURE) {
                final Uri resultUri = EasyCamera.getOutput(data);
                int width = EasyCamera.getImageWidth(data);
                int height = EasyCamera.getImageHeight(data);


                Timber.i("imageWidth: %d", width);
                Timber.i("imageHeight: %d", height);

                mImageView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadPreview(resultUri);
                        getPresenter().startOCR(resultUri);
                    }
                }, 300);
            }
        }
    }

    @Override
    public void submitText(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mExtractedText.setText(Html.fromHtml(text));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        menu.clear();
        getMenuInflater().inflate(R.menu.share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.ab_share){
            PopupMenu menu = new PopupMenu(this, findViewById(R.id.ab_share));
            for (String word : getPresenter().getFoundWords()) {
                menu.getMenu().add(word);
            }
            if(!getPresenter().getFoundWords().isEmpty()) {
                menu.getMenu().add(getString(R.string.share_all));
            }
            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if(item.getTitle().toString().equals(getString(R.string.share_all))){
                        setShareIntent(getPresenter().getFoundLinks());
                    } else {
                        setShareIntent(item.getTitle().toString());
                    }
                    return true;
                }
            });
            menu.show();
        }
        return true;
    }

    // Call to update the share intent
    private void setShareIntent(String shareWord) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareWord);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void setShareIntent(List<String> shareWord) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        StringBuilder shareText = new StringBuilder();
        for (String s : shareWord) {
            shareText.append(s);
        }
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void showLoading(final boolean showLoading) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (showLoading) {
                    mProgressBar.setVisibility(View.VISIBLE);
                } else {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void loadPreview(final Uri imageUri) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Picasso.with(OcrActivity.this).load(imageUri).fit().into(mImageView);
            }
        });
    }
}
