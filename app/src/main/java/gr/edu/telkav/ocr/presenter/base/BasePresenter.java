package gr.edu.telkav.ocr.presenter.base;

import android.support.annotation.Nullable;

import gr.edu.telkav.ocr.view.BaseViewContract;

public class BasePresenter<View extends BaseViewContract> {
    private View mView;

    public final void attachView(View view){
        mView = view;
        onAttachView();
    }

    public final void detachView(){
        mView = null;
        onDetachView();
    }

    public void onAttachView(){}

    public void onDetachView(){}

    @Nullable
    public View getView() {
        return mView;
    }
}
