/*
 * Copyright (C) 2008 ZXing authors
 * Copyright 2011 Robert Theis
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package gr.edu.telkav.ocr.view.camera2;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

import gr.edu.telkav.ocr.R;

/**
 * apo ZXing project: http://code.google.com/p/zxing
 */
public final class ViewfinderView extends View {

    private final Paint paint;
    private final int maskColor;
    private final int frameColor;
    private final int cornerColor;
    //  Rect bounds;
    private Pair<Rect, Point> previewFrame;


    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Initialize these once for performance rather than calling them every time in onDraw().
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.viewfinder_mask);
        frameColor = resources.getColor(R.color.viewfinder_frame);
        cornerColor = resources.getColor(R.color.viewfinder_corners);
    }

    public void setPreviewFrame(Pair<Rect, Point> previewFrame) {
        this.previewFrame = previewFrame;
    }

    @SuppressWarnings("unused")
    @Override
    public void onDraw(Canvas canvas) {
        if (previewFrame == null) {
            return;
        }

        Rect frame = previewFrame.first;

        int width = previewFrame.second.x;
        int height = previewFrame.second.y;

        //readjust the crop view to the crop size of the image that is in different ratio
        frame.top *= Float.parseFloat(String.valueOf(getHeight()))  / previewFrame.second.y;
        frame.bottom *= Float.parseFloat(String.valueOf(getHeight()))  / previewFrame.second.y;
        frame.left *= Float.parseFloat(String.valueOf(getWidth()))  / previewFrame.second.x;
        frame.right *= Float.parseFloat(String.valueOf(getWidth()))  / previewFrame.second.x;

        //invert the view bounds so that frame is basically mirrored on y axis
        int frameHeight = frame.top - frame.bottom;
        frame.top += frameHeight;
        frame.bottom += frameHeight;
        // Draw the exterior (i.e. outside the framing rect) darkened
        paint.setColor(maskColor);
        canvas.drawRect(0, 0, getWidth(), frame.top, paint);
        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
        canvas.drawRect(frame.right + 1, frame.top, getWidth(), frame.bottom + 1, paint);
        canvas.drawRect(0, frame.bottom + 1, getWidth(), getHeight(), paint);

        // Draw a two pixel solid border inside the framing rect
        paint.setAlpha(0);
        paint.setStyle(Style.FILL);
        paint.setColor(frameColor);
        canvas.drawRect(frame.left, frame.top, frame.right + 1, frame.top + 2, paint);
        canvas.drawRect(frame.left, frame.top + 2, frame.left + 2, frame.bottom - 1, paint);
        canvas.drawRect(frame.right - 1, frame.top, frame.right + 1, frame.bottom - 1, paint);
        canvas.drawRect(frame.left, frame.bottom - 1, frame.right + 1, frame.bottom + 1, paint);

        // Draw the framing rect corner UI elements
        paint.setColor(cornerColor);
        canvas.drawRect(frame.left - 15, frame.top - 15, frame.left + 15, frame.top, paint);
        canvas.drawRect(frame.left - 15, frame.top, frame.left, frame.top + 15, paint);
        canvas.drawRect(frame.right - 15, frame.top - 15, frame.right + 15, frame.top, paint);
        canvas.drawRect(frame.right, frame.top - 15, frame.right + 15, frame.top + 15, paint);
        canvas.drawRect(frame.left - 15, frame.bottom, frame.left + 15, frame.bottom + 15, paint);
        canvas.drawRect(frame.left - 15, frame.bottom - 15, frame.left, frame.bottom, paint);
        canvas.drawRect(frame.right - 15, frame.bottom, frame.right + 15, frame.bottom + 15, paint);
        canvas.drawRect(frame.right, frame.bottom - 15, frame.right + 15, frame.bottom + 15, paint);


        // Request another update at the animation interval, but don't repaint the entire viewfinder mask.
        //postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top, frame.right, frame.bottom);
    }

    public void drawViewfinder() {
        invalidate();
    }

}
