package gr.edu.telkav.ocr.view.screens.ocr;

import android.net.Uri;

import gr.edu.telkav.ocr.view.BaseViewContract;

public interface OcrView extends BaseViewContract {
    void submitText(String text);

    void showLoading(boolean showLoading);

    void loadPreview(Uri imageUri);
}
