package gr.edu.telkav.ocr.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import gr.edu.telkav.ocr.presenter.base.BasePresenter;

/**
 * Presenter Activity attaching and detaching presenter when it becomes visible {@link #onStart()}
 * and when Activity becomes invisible {@link #onStop()}
 *
 * @param <P> Presenter class for given activity
 */
public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseViewContract{
    private P mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
    }

    protected abstract P createPresenter();

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detachView();
    }

    public P getPresenter() {
        return mPresenter;
    }

    @Override
    public Context getContext() {
        return this;
    }
}
