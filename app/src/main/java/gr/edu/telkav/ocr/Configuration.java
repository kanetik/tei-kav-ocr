package gr.edu.telkav.ocr;

public class Configuration {
    public static final String LANGUAGE = "ell";
    public static final String DATA_PATH = App.getInstance().getFilesDir().toString() + "/TesseractSample/";

    public static String getUrlQuery(String query) {
        return "<a href='https://www.google.gr/search?q=" + query + "'>" + query + "</a>";
    }

    public static String getOnlyUrlQuery(String query) {
        return "https://www.google.gr/search?q=" + query;
    }


    public enum WORDS_MATCHER {
        V_123_1234("(ν\\.\\s*\\n*)[0-9]{1,4}(\\/)[0-9]{4}"),
        FEK_1_12_12_1234_tA("(\\(ΦΕΚ\\s*\\n*)[0-9]{1,4}(\\/)[0-9]{1,2}(-)[0-9]{1,2}(-)[0-9]{4}(\\/)(τ\\.[Α|Β]([΄])\\))"),
        //ΦΕΚ 106/Α΄/1987
        FEK_132_A_1997("(\\ΦΕΚ\\s*\\n*)[0-9]{1,4}(\\/)([Α|Β]([΄]))(\\/)[0-9]{4}"),
        //ΦΕΚ 51/Α΄/10-4-1997
        FEK_123_A_12_1_1997("(\\ΦΕΚ\\s*\\n*)[0-9]{1,4}(\\/)([Α|B]([΄]))(\\/)[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}"),
        //ΦΕΚ Α΄ 114/2015
        FEK_A_123_2015("(\\ΦΕΚ\\s*\\n*)([Α|Β]([΄]))(\\s*\\n*)[0-9]{1,4}\\/[0-9]{4}"),
        //ΦΕΚ 182 Α΄
        FEK_123_A("(\\ΦΕΚ\\s*\\n*)[0-9]{1,4}(\\s*\\n*)([Α|Β]([΄]))"),
        //ΦΕΚ Α΄ 118
        FEK_A_123("(\\ΦΕΚ\\s*\\n*)([Α|Β]([΄]))(\\s*\\n*)[0-9]{1,4}"),
       //Ν. 3889/2010
        N_1234_2010("(\\Ν.\\s*\\n*)[0-9]{1,4}(\\/)[0-9]{4}");

        private String mRegex;

        WORDS_MATCHER(String regex) {
            mRegex = regex;
        }

        public String getRegex() {
            return mRegex;
        }
    }
}
